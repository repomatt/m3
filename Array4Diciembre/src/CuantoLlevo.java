import java.util.Scanner;

public class CuantoLlevo {


  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int num1;
    int num2;
    num1 = scan.nextInt();
    num2 = scan.nextInt();
    while (!((num1==0) && (num2==0))) {
      int res1 = 0;
      int res2 = 0;
      int llevar = 0;
      while ((num1 > 0) && (num2 > 0)) {
        res1 = num1 % 10;
        res2 = num2 % 10;
        if (res1 + res2 > 9) {
          llevar++;
        }
        num1 = num1 / 10;
        num2 = num2 / 10;
      }
      System.out.println(llevar);
      num1 = scan.nextInt();
      num2 = scan.nextInt();
    }
  }
}

