import java.util.Scanner;

public class Matriz {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Numero de filas");
    int fila = scan.nextInt();
    System.out.println("Numero de columnas");
    int columna = scan.nextInt();
    int[][] matriz = new int[fila][columna];
    boolean esbueno = true;
    while (columna == fila && esbueno && !(columna == 0) && !(fila == 0)) {
      for (int i = 0; i < fila; i++) {
        System.out.println("Introduzca el contenido");
        for (int j = 0; j < columna; j++) {
          matriz[i][j] = scan.nextInt();
        }
      }
      for (int i = 0; i < fila; i++) {
        for (int j = 0; j < columna; j++) {
          if (!(matriz[i][i] == 1)) {
            esbueno = false;
          }
        }
      }
      if (esbueno) {
        System.out.println("SI");
      } else {
        System.out.println("NO");
      }
      System.out.println("Numero de filas");
      fila = scan.nextInt();
      System.out.println("Numero de columnas");
      columna = scan.nextInt();
    }
  }
}