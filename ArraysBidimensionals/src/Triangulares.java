import java.util.Scanner;



public class Triangulares {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int fila = scan.nextInt();
    int columna = scan.nextInt();
    boolean esbueno = true;
    while (columna == fila && esbueno && !(columna == 0) && !(fila == 0)) {

      int[][] matriz = new int[fila][columna];
      for (int i = 0; i < fila; i++) {
        for (int j = 0; j < columna; j++) {
          matriz[i][j] = scan.nextInt();
        }
      }
      for (int i = 0; i < fila; i++) {
        for (int j = 0; j < columna; j++) {


          if (matriz[1][0] == 0 && esbueno) {
            for (int p = 1; p < fila - 1; p++) {
              for (int h = 1; h < fila - 1; h++) {
                esbueno = matriz[p][h - 1] == 0;
              }
            }
          }

          if (matriz[0][1] == 0 && esbueno) {
            for (int p = 0; p < fila - 1; p++) {
              for (int h = 0; h < fila - 1; h++) {
                esbueno = matriz[p][h + 1] == 0;
              }
            }
          }
          else {
            esbueno = false;
          }
        }
      }
      System.out.println(esbueno);
      esbueno = true;
      fila = scan.nextInt();
      columna = scan.nextInt();
    }
  }
}
