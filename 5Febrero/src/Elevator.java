import java.util.Arrays;
import java.util.Scanner;

public class Elevator {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int total = 0;
    String numbers = scan.nextLine();
    String numsString[] = numbers.split(" ");
    int nums[] = new int[numsString.length-1];
    for (int i = 0; i< numsString.length-1; i++){
      nums[i] = Integer.parseInt(numsString[i]);
    }
    for (int i = 0; i< nums.length-1; i++){
      total += Math.abs(nums[i] - nums[i+1]);
    }
    System.out.println(total);
  }
}
