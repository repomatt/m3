import java.util.Scanner;

public class Minas {
  public static boolean outofBounds(int x, int y, int largo, int ancho) {
    boolean outofBounds = (x < 0 || x > largo - 1 || y < 0 || y > ancho - 1);
    return outofBounds;
  }

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int count = 0;
    int times = 0;
    int columnas = Integer.parseInt(scan.nextLine());
    int filas = Integer.parseInt(scan.nextLine());
    String[][] camp = new String[filas][columnas];
    for (int i = 0; i < filas; i++) {
      for (int k = 0; k < columnas; k++) {
        camp[i][k] = scan.next();
      }
    }

    for (int i = 0; i < filas; i++) {
      for (int k = 0; k < columnas; k++) {
        if (camp[i][k].equals("-")) {

            for (int q = k - 1; q <= k + 1; q++) {

              if (!outofBounds(i-1,q,filas,columnas) && camp[i - 1][q].equals("*")) {
                count++;
              }
              if (!outofBounds(i+1,q,filas,columnas) && camp[i + 1][q].equals("*")) {
                count++;
              }
            }
          if (!outofBounds(i,k-1,filas,columnas) && camp[i][k-1].equals("*")){
            count++;
          }

          if (!outofBounds(i,k+1,filas,columnas) && camp[i][k+1].equals("*")){
            count++;
          }
          if (count>=6){
            times++;
          }

        }
        count = 0;
      }
    }
    System.out.println(times);
  }
}
