import java.util.Scanner;
public class Mario {
public static int jumpsDown(int[] array){
  int downs = 0;
    for (int i = 0; i < array.length-1; i++) {
      if ((array[i]) > (array[i + 1])) {
        downs++;
      }
    }
  return downs;
}
  public static int jumpsUp(int[] array){
    int ups = 0;
    for (int i = 0; i < array.length - 1; i++) {
      if ((array[i]) < (array[i + 1])) {
        ups++;
      }
    }
    return ups;
  }
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("casos");
    int casos = Integer.parseInt(scan.nextLine());
    for (int o = casos; o>0; o--) {
      System.out.println("Cuanto mide el array?");
      int[] JumpsArray = new int[Integer.parseInt(scan.nextLine())]; // Declare array
      // This will fill in the array with how many jumps
      for (int i = 0; i < JumpsArray.length; i++) {
        JumpsArray[i] = Integer.parseInt(scan.nextLine());
      }
      System.out.println("Arriba: " + jumpsUp(JumpsArray));
      System.out.println("Abajo: " + jumpsDown(JumpsArray));
    }
  }
}
