import java.util.Arrays;
import java.util.Scanner;

public class Ohms {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int ohmsTotal = 0;
    String input = scan.nextLine();
    String[] array = input.split(" ");
    for (int i = 0; i < array.length; i++) {
      if (array[i].equals("o")) {
        ohmsTotal += Integer.parseInt(array[i - 1]);
      }
      if (array[i].equals("da")) {
        int j = Integer.parseInt(array[i - 1]);
        ohmsTotal += j * 10;
      }
      if (array[i].equals("h")) {
        int j = Integer.parseInt(array[i - 1]);
        ohmsTotal += j * 10 * 10;
      }
      if (array[i].equals("k")) {
        int j = Integer.parseInt(array[i - 1]);
        ohmsTotal += j * 10 * 10 * 10;
      }
      if (array[i].equals("M")) {
        int j = Integer.parseInt(array[i - 1]);
        ohmsTotal += j * 10 * 10 * 10 * 10;
      }
      if (array[i].equals("G")) {
        int j = Integer.parseInt(array[i - 1]);
        ohmsTotal += j * 10 * 10 * 10 * 10 * 10;
      }
    }
    System.out.println(ohmsTotal+" o");
    int numberFinal = ohmsTotal;
    int counter = 0;
    while(numberFinal%10==0){
     numberFinal/=10;
     counter++;
    }
    if (counter == 1){
      System.out.println(numberFinal+" da");
    }
    else if (counter == 2){
      System.out.println(numberFinal+" h");
    }
    else if (counter == 3){
      System.out.println(numberFinal+" k");
    }
    else if (counter == 4){
      System.out.println(numberFinal+" M");
    }
    else if (counter == 5){
      System.out.println(numberFinal+" G");
    }
    else {
      System.out.println(ohmsTotal+" o");
    }


  }
}
