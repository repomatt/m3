
import java.util.Scanner;
public class Montana {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int montanas = scan.nextInt();
    while (montanas != 0) {
      int[] list = new int[montanas+1];
      list[list.length-1] = 0;
      int count = 0;
      for (int i = 0; i < montanas; i++) {
        list[i] = scan.nextInt();

      }
      for (int i = 0; i < list.length-1; i++) {
        if (list[i] > list[i + 1]) {
          count++;
        }
      }
      System.out.println(count);
      montanas = scan.nextInt();

    }
  }
}
