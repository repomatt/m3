
import java.util.Scanner;

public class Vampiro {
  private static boolean parImpar(String input){
    return input.length() % 2 == 0;
  }

  public static String vampiro(String numero) {
    String response = "NO";
    if (parImpar(numero)) {
      String[] numToArray = numero.split("");
      String firstHalf = numero.substring(0,numero.length()/2);
      String secondHalf = numero.substring((numero.length()/2),numero.length());


      int result = Integer.parseInt(firstHalf) * Integer.parseInt(secondHalf);
      String compare = String.valueOf(result);
      boolean vampirico = true;

        for (int i = 0; i < numero.length(); i++) {
          if (compare.contains(numToArray[i])) {
            response = "SI";
          } else {
            response = "NO";
            break;
          }
        }
      }

    return response;
  }

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    String num = scan.nextLine();
    System.out.println(vampiro(num));

  }
}
