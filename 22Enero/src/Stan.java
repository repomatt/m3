import java.util.Scanner;

public class Stan {
  public static int findStan(String frase){
    int result = 0;
    int largo = frase.length();
    String[] stanArray = {"s","t","a","n","l","e","e"};

      for (int i = 0; i < stanArray.length; i++) {
        result = frase.indexOf(stanArray[i]);
        if (result != -1) {
          frase = frase.substring(result);
        }
        if (result == -1) {
          break;
        }
      }
   if (result != -1){

     result = largo-frase.length();
   }
    return result;
  }
  public static int countStans(String frase){
    int position = findStan(frase);
    int counter = 0;
    while (position != -1){
      counter++;
      frase = frase.substring(position);
      position = findStan(frase);
    }

    return counter;
  }

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    String frase = scan.nextLine();
    frase = frase.toLowerCase();
    System.out.println(countStans(frase));
  }
}
