import java.util.Scanner;

public class Verbs {
public static String[] verbosFunc(String palabra, String verbo, String[] verb){
  if (palabra.substring(palabra.length()-2).equals("ar") && verbo.equals("A")){
    palabra = palabra.substring(0,palabra.length()-2);
    verb[0] = palabra+"o";
    verb[1]= palabra+"as";
    verb[2] = palabra+"a";
    verb[3] = palabra+"amos";
    verb[4] = palabra+"ais";
    verb[5] = palabra+"ais";

  }
  if (palabra.substring(palabra.length()-2).equals("ar") && verbo.equals("F")){
    palabra = palabra.substring(0,palabra.length()-2);
    verb[0] = palabra+"are";
    verb[1]= palabra+"aras";
    verb[2] = palabra+"ara";
    verb[3] = palabra+"aremos";
    verb[4] = palabra+"areis";
    verb[5] = palabra+"aran";

  }  if (palabra.substring(palabra.length()-2).equals("ar") && verbo.equals("P")){
    palabra = palabra.substring(0,palabra.length()-2);
    verb[0] = palabra+"e";
    verb[1]= palabra+"aste";
    verb[2] = palabra+"o";
    verb[3] = palabra+"amos";
    verb[4] = palabra+"eis";
    verb[5] = palabra+"aron";

  }
  if (palabra.substring(palabra.length()-2).equals("er") ||palabra.substring(palabra.length()-2).equals("ir") && verbo.equals("A")){
    palabra = palabra.substring(0,palabra.length()-2);
    verb[0] = palabra+"o";
    verb[1]= palabra+"es";
    verb[2] = palabra+"e";
    verb[3] = palabra+"emos";
    verb[4] = palabra+"eis";
    verb[5] = palabra+"en";

  }
  if (palabra.substring(palabra.length()-2).equals("er") ||palabra.substring(palabra.length()-2).equals("ir") && verbo.equals("P")){
    palabra = palabra.substring(0,palabra.length()-2);
    verb[0] = palabra+"i";
    verb[1]= palabra+"iste";
    verb[2] = palabra+"io";
    verb[3] = palabra+"imos";
    verb[4] = palabra+"isteis";
    verb[5] = palabra+"ieron";

  }
  if (palabra.substring(palabra.length()-2).equals("er") ||palabra.substring(palabra.length()-2).equals("ir") && verbo.equals("F")){
    palabra = palabra.substring(0,palabra.length()-2);
    verb[0] = palabra+"ere";
    verb[1]= palabra+"eras";
    verb[2] = palabra+"era";
    verb[3] = palabra+"eremos";
    verb[4] = palabra+"ereis";
    verb[5] = palabra+"eran";

  }
return verb;
}
  public static void main(String[] args) {
    String[] verb = new String[6];
    String palabra = "vivir";
    String verbo = "P";
    verbosFunc(palabra,verbo,verb);
    System.out.println("Yo "+verb[0]);
    System.out.println("Tu "+verb[1]);
    System.out.println("El "+verb[2]);
    System.out.println("Nosotros "+verb[3]);
    System.out.println("Vosotros "+verb[4]);
    System.out.println("Ellos "+verb[5]);

  }
}
