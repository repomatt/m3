import java.util.Scanner;

public class Fractales {
  public static int contarFractales(int x){
    if ( x == 1){
      return 4;
    }else  return x*4+4*contarFractales(x/2);
  }

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int x = scan.nextInt();
    System.out.println(contarFractales(x));
  }
}
