import java.util.Scanner;
public class Capicuas {

  public static int countNumbers(int x){
    if (x == 1) return 10;
    if (x %2 == 0){
      x = (x -1)/ 2;
    }else x = x/2;
    int potencia = (int)Math.pow(10,x)*9;
    return potencia;
  }
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int num = scan.nextInt();
    System.out.println(countNumbers(num));
  }
}
